/*!
  * Bootstrap 4 Modal Loader
  * Copyright Stefan Piffl (https://www.drpiffl.de)
  * Licensed under MIT (https://gitlab.com/DrPiffl/bs-modal-load/blob/master/LICENSE)
  */
( function( $ ) {
    $.fn.modalLoad = function( url ) {
        // load modal directly // $.fn.modalLoad('modal/modal.html');
        if( typeof url === 'string' ) {
            return modalLoad( url );
        }

        // load modal on click // data-toggle="modalLoad" data-url="modal/modal.html"
        this.click( function() {
            return modalLoad( $( this ).data( 'url' ) );
        } );

        // load and show modal from url
        function modalLoad( url ) {
            $( '#modalLoad' ).remove();
            $( 'body' ).append( '<div id="modalLoad"></div>' );

            if( typeof url === 'string' && url !== '' ) {
                $( '#modalLoad' ).load( url, function( response, status ) {
                    if( status === 'success' ) {
                        $( '.modal', this ).removeClass( 'fade' ).modal( { backdrop: 'static' } );
                        return true;
                    }
                } );
            }
            return false;
        }
    };

    $( function() {
        // register on DOM content loaded
        $( '*[data-toggle="modalLoad"]' ).modalLoad();

        // register on DOM node inserted
        $( document ).on( 'DOMNodeInserted', '* > *[data-toggle="modalLoad"]', function() {
            $( this ).modalLoad();
        } );
    } );
}( jQuery ) );